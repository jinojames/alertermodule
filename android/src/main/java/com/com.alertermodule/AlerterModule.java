package com.alertermodule;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.tapadoo.alerter.Alerter;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.tapadoo.alerter.OnHideAlertListener;

public class AlerterModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;

    public AlerterModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "AlerterModule";
    }

    private void sendEventToJavaScript(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    @ReactMethod
    public void createAlert() {
        WritableMap params = Arguments.createMap();
        params.putString("onHide", "Native to JS Bridging Works");
        Alerter.create(reactContext.getCurrentActivity())
                .setTitle("Hey jino !")
                .setText("Bridging done..")
                .setOnHideListener(new OnHideAlertListener() {
                    @Override
                    public void onHide() {
                        sendEventToJavaScript(reactContext, "OnAlertHide", params);
                    }
                })
                .show();
    }
}